<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  version="1.0" xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <h1>
                <img>
                    <xsl:attribute name="src">
                        <xsl:value-of select="/kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                    </xsl:attribute>
                </img>
                <xsl:value-of select="//kml:name"/>
            </h1>
            <p>List of hotels</p>
            <ul> 
                <xsl:for-each select="//kml:Placemark">
                    <xsl:param name="url" select="kml:description"/>
                    <xsl:sort select="kml:name" order="ascending"/>
                    <li>
                        <xsl:value-of select="kml:name"/>
                        <ul>
                            <li>
                                <a>
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="substring-before(substring-after($url,'&#34;'),'&#34;')"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="substring-before(substring-after($url,'&#34;'),'&#34;')"/>
                                </a>
                            </li>   
                            <li>Coordinates:
                                <xsl:value-of select="kml:Point/kml:coordinates"/>
                            </li>      
                        </ul>
                    </li>
                </xsl:for-each>
            </ul>
        </html>
    </xsl:template>
</xsl:stylesheet>
