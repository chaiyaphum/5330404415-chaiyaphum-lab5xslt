<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>twitter.html</title>
            </head>
            <body>
                <h1>Twitter @
                    <a>
                        <xsl:attribute name="href"> 
                            <xsl:value-of select="rss/channel/link"/> 
                        </xsl:attribute>
                        <xsl:value-of select="rss/channel/link"/> 
                    </a>
                </h1>        
                <table border="1">
                    <tr>
                        <th width="30">Title</th>
                        <th width="20">Publication Date</th>
                    </tr>
                    <xsl:for-each select="rss/channel/item">
                        <tr>
                            <td>
                                <a>
                                    <xsl:attribute name="href"> 
                                        <xsl:value-of select="link"/> 
                                    </xsl:attribute>
                                    <xsl:value-of select="title"/> 
                                </a>
                            </td>
                            <td>
                                <xsl:value-of select="pubDate"/> 
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
