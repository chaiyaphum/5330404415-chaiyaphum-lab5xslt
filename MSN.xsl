<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:variable name="firstUser" select="//Message[1]/From/User/@FriendlyName"/>
    <xsl:template match="/">
        <html>
            <head>
                <META http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
                <meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
                <title>Kanda Saikaew' s MSN Log</title>
                <style type="text/css">
                    body {font-family: Verdana, arial, sans-serif;}         
                </style>
            </head>
            <body>
               
                <span style="color: red">
                    [Conversation started on
                    <xsl:value-of select="Log/Message[1]/@Date"/>&#160;
                    <xsl:value-of select="Log/Message[1]/@Time"/>]
                </span>
                <br/>
                
                <table border="0">
                    <xsl:for-each select="Log/Message">
                        <tr>
                            <td>[
                                <xsl:value-of select="@Date"/>&#160;
                                <xsl:value-of select="@Time"/>]
                            </td>
                            <xsl:choose>
                                <xsl:when test="contains(From/User/@FriendlyName, $firstUser)">
                                    <td>
                                        <span style="color: #FFAA00"> 
                                            <xsl:value-of select="From/User/@FriendlyName"/>
                                        </span>
                                    </td>
                                    <td>:</td>
                                    <td>
                                        <span style="color: #FFAA00"> 
                                            <xsl:value-of select="Text"/>
                                        </span>
                                    </td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td>
                                        <span style="color: #24913C"> 
                                            <xsl:value-of select="From/User/@FriendlyName"/>
                                        </span>
                                    </td>
                                    <td>:</td>
                                    <td>
                                        <span style="color: #24913C"> 
                                            <xsl:value-of select="Text"/>
                                        </span>
                                    </td>
                                </xsl:otherwise>
                            </xsl:choose>
                        </tr>    
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>














